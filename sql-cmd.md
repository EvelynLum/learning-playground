- `exec sp_columns [table-name]`
- `sp_helptext [view-name]`
- Get foreign keys
    ```sql
    SELECT f.name AS foreign_key_name
           ,OBJECT_NAME(f.parent_object_id) AS table_name
           ,COL_NAME(fc.parent_object_id, fc.parent_column_id) AS constraint_column_name
           ,OBJECT_NAME (f.referenced_object_id) AS referenced_object
           ,COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS referenced_column_name
    FROM sys.foreign_keys AS f
    INNER JOIN sys.foreign_key_columns AS fc
    ON f.object_id = fc.constraint_object_id
    WHERE f.parent_object_id = OBJECT_ID('HumanResources.Employee');
    ```

```bat
-- Configs
sqlcmd -S 172.17.80.97 -U ifca.reporting -P P@ssw0rd -d SPICES_THREE

-- Other options
-i "C:\Users\evelynlum\Desktop\RenameCF_BankID.sql"
-o "C:\Users\evelynlum\Desktop\20200311.txt"

-- Example
sqlcmd -S 172.17.80.97 -U ifca.reporting -P P@ssw0rd -d SPICES_THREE -i "C:\Users\evelynlum\Desktop\RenameCF_BankID.sql" -o "C:\Users\evelynlum\Desktop\20200311.txt"
```
