`console.log('Hello JS');`

### Variables
- Cannot start with a number
- Cannot contain a space or hyphen (-)
```js
let age;
age = 30;

let a, b, c;
a=1;

let a = 1, b = 2, c = 3;

console.log(age);
```

### Constants
- Read only
```js
const pi = 3.14;
```

### Primitive data type
```js
let name = 'John'; // String Literal
let age = 30; // Number Literal
let loveCat = true; // Boolean Literal
let color = null; // Null
let list; // Undefined

// Template Literals
let message = `${name} is ${age}`;
```

```js
// Array Literals
const myArray = ['red',4, true, null, [1,2,3]];

const numbers = new Array(1,2,3,4,5,6);
const numbers = [1,2,3,4,5,6];
numbers[2] = 9;
console.log(numbers[2]);
numbers[6] = 7;
numbers.push(8);
let lastNumber = numbers.pop(); // 8
let firstNumber = numbers.shift(); // 1
```

```js
// Object Literals, KeyValue pair
const person = {
    firstName: 'John',
    lastName: 'Smith',
    age: 30,
    pets: ['cat', 'dog'],
    address: {
        city: 'Houston',
        state: 'Texas'
    }
}

person.email = 'john@email.com';
delete person.age;

// Dot Notation
console.log(person.age);

// Bracket Notation
console.log(person['age']);

//// Pulling out certain value from object
const { firstName, lastName, address: {city} } = person;
console.log(firstName, lastName, city);
```

```js
// Object Literals, KeyValue pair
const people = [
    {
        firstName: 'John',
        lastName: 'Smith',
        age: 30,
        pets: ['cat', 'dog'],
        address: {
            city: 'Houston',
            state: 'Texas'
    },
    {
        firstName: 'Mary',
        lastName: 'Smith',
        age: 25,
        pets: ['cat', 'dog'],
        address: {
            city: 'Houston',
            state: 'Texas'
        }
    }
]

const peopleJSON = JSON.stringify(people);

console.log(JSON.parse(peopleJSON));
```

### Functions
```js
// Functions
function convertPercent(num){
    return num/100;
}

console.log(convertPercent(100));
```

```js
// Arrow Function
const convertPercent = (num) => {
    return number/100;
}

//// Since only 1 param, the brancket can be removed; 
//// Since only 1 line, the braces and return can be removed.
const convertPercent = num => number/100;

console.log(convertPercent(100));
```

```js
let message = () => 'Hello World!';
```

```js
// function message(a) {
//     return `Hello ${a}`;
// }

let message = (a, b) => return `Hello ${a} ${b}`;
console.log(message('My', 'Dear'));
```

### Loops
```js
// Foreach loop
const people = [
    {
        firstName: 'John',
        lastName: 'Smith',
        age: 30,
        pets: ['cat', 'dog'],
        address: {
            city: 'Houston',
            state: 'Texas'
    },
    {
        firstName: 'Mary',
        lastName: 'Smith',
        age: 25,
        pets: ['cat', 'dog'],
        address: {
            city: 'Houston',
            state: 'Texas'
        }
    }
]

for (let person of perople) {
    console.log(person.age);
}

// High order array method
people.forEach(function(person){
    console.log(person.age);
})

// High order array method with Arrow function
people.forEach(person => console.log(person.age));

// High order array method with Arrow function with map
const personAge = people.map(person => person.age);

// Filter
const personAgeUnder30Age = people.filter(person => person.age < 30);
console.log(personAgeUnder30Age); // Result: Mary only
```

### DOM - Document Object Model
```html
<html>
    <head>
        <title>JavaScript Crash Course</title>
    </head>
    <body>
        <ul id="list">
            <li class="list-item">Item 1</li>
            <li class="list-item">Item 2</li>
            <li class="list-item">Item 3</li>
        </ul>
    </body>
</html>
```

```js
// Show the whole html object

// Single Element Selectors
const list = document.getElementById('list');
const list = document.querySelector('ul');
const list = document.querySelector('#list');

// Multiple Element Selectors
const listItems = document.querySelectorAll('.list-item');

listItems.forEach(item => {
    console.log(item.textContent);
})

// Methods for dynamic web page manipulation
list.firstElementChild.remove();
list.lastElementChild.remove();
list.firstElementChild.textContent = "New item";
list.firstElementChild.innerHTML = "<h1>New item</h1>";
list.lastElementChild.style.background = 'red';

console.log(listItems);
```

### Events
```html
<html>
    <head>
        <title>JavaScript Crash Course</title>
    </head>
    <body>
        <button id="myBtn">Click Me</button>
    </body>
</html>
```

```js
// Events
const btn = document.getElementById('myBtn');

btn.addEventListener('click', function(e){
    console.log(e);
})

// Arrow function style
btn.addEventListener('click', (e) => console.log(e))

// Other controls
btn.addEventListener('mouseover', function(e){
    btn.style.background = 'blue';
})
```

###  Sample of Add task
```html
<html>
    <head>
        <title>JavaScript Crash Course</title>
    </head>
    <body>
        <button id="myBtn">Click Me</button>
        <template id="task-template">
            <li class="task">
                <input type="checkbox">
                <label>
                    <span class="check-mark">&#10004;</span>
                    <span class="custom-checkbox"></span>
                </label>
            </li>
        </template>
    </body>
</html>
```

```js
let id = 1;
function addTask() {
    const taskElement = document.importNode(taskTemplate.content, true);
    
    const checkbox = taskElement.querySelector('input');
    checkbox.id = id;
    
    const label = taskElement.querySelector('label');
    label.htmlFor = id;
    label.append(newTask.value);
    
    taskList.appendChild(taskElement);
    newTask.value = '';
    
    id++;
}

removeCompleteBtn.addEventListener('click', () => {
    const tasks = document.querySelectorAll('.task');
    
    tasks.forEach(task => {
        const checked = task.querySelector('input').checked;
        if(checked){
            task.remove();
        }
    })
});
```