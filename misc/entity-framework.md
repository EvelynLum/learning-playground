## Self Understanding (might wrong, get better over time)
- DbContext: Database
- Entity: Table
- Model: Row
- Collection: Rows
- 


> Entity Data Model (EDM)
- An in-memory representation of the entire metadata: conceptual model, storage model, and mapping between them.

> Unit Of Work
An instance of the context class represents Unit Of Work and Repository patterns wherein it can combine multiple changes under a single database transaction.

___________ in Visual Studio displays conceptual model, storage model and mapping information of EDM.

DbContext
DbSet
Entity
DbSession
ObjectContext

> 
- An instance of __________ class represents a session with the underlying database.

> DbSet
- Class represents an entity set

> 
- Class provides information about an existing entity in a context.

> 
- Valid entity states in EF 6

> 
- Executes a raw SQL query to the database in EF 6

> myDbContext.Configuration.ProxyCreationEnabled = false;
- Disable POCO Proxy creation using DBContext

> myDBContext.Configuration.LazyLoadingEnabled = false;
- Disable Lazy loading using DBContext?

> Include()
- Eager loading in EF 6.

> Load()
- Explicit loading in EF 6.

> ()
- Returns a new query where the entities returned will not be cached in the DbContext

> Attach()
- Attaches an entity to a context with Unchanged entity state?
