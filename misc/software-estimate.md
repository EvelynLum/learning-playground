Hofstadter's Law: It always takes longer than you expect, even when you take into account Hofstadter's Law.

> Man-month myth
- Fall prey to the man-month myth where you think people and effort are interchangeable.
Adding new people to a delayed project almost always causes it to slow even further because of communication and training.

> Planning poker, Edge-cases
- Allows more people to identify edge-cases, bring their experience, and have more ownership of the final number.

> Low-level and High-level estimation
> 
