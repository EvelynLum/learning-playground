- brew info redis
- brew install redis
- redis-cli ping

### Start server (Either one)
- redis-server
- ln -sfv /usr/local/opt/redis/*.plist ~/Library/LaunchAgents
- launchctl load ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
- redis-server /usr/local/etc/redis.conf

### Stop Redis on autostart on computer start.
- redis-cli shutdown
- launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
- Configure at Redis configuration file (/usr/local/etc/redis.conf)

### Uninstall Redis and its files.
- brew uninstall redis
- rm ~/Library/LaunchAgents/homebrew.mxcl.redis.plist
