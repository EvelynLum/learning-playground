http://127.0.0.1:8000/dashboard/configs/showUpdatePowerbarButtonsConfig

http://127.0.0.1:8000/lobby/1

### Serve
php artisan serve

### Database
- php artisan migrate
- php artisan make:seed {{seeder-name}}
- php artisan db:seed
- php artisan migrate:refresh --seed
- php artisan migrate --seed

### Event and Listener
- php artisan event:generate
- php artisan make:event PowerbarButtonUpdatedEvent
- php artisan make:listener RenderPowerbarButtons --event=PowerbarButtonUpdatedEvent

https://laracasts.com/series/laravel-6-from-scratch
```php
$limit = $request->limit ?? 25;
$limit = $request->get('limit', 25); // 2nd param is defaultValue if limit doesnt exist
$limit = $request->query('limit', 25);
```

```php
{{-- HTML::form("foo") --}};

<?php /* 
{{ HTML::form("foo") }};
{{ HTML::form("bar") }};
*/ ?> 
```

```php
-- blade.php
<meta name="lobby_id" content="{{ $lobbyId }}">
<meta name="number_of_participants" content="{{ $numberOfParticipants }}">

-- In Vue
<script>
export default {
  props: ["lobby_id", "number_of_participants"],
  data() {
    return {
      lobbyId: $('meta[name="lobby_id"]').attr("content"),
      numberOfParticipants: $('meta[name="number_of_participants"]').attr("content"),
    };
  },
};
</script>
```

### Build js and css files
npm run production

### Tumpang
- redis-cli ping
- redis-server
- redis-cli shutdown

### Run socket.io in Laravel
- npm run echo-server

### Create project
- laravel new {{project-name}}
- composer create-project laravel/laravel {{project-name}}

### Installation
- Install composer
- Install artisan
- (TODO: Need to install others if not mistaken)

### When something went wrong
composer global update