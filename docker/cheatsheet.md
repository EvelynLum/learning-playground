## docker build (compile) -> run (test locally) -> publish (publish to public)
1. `docker build -t [img-output-file-name]:[img-tag-name] [location-of-build-file]`
    eg. `docker build -t go-local:0.1 .`
    Docker build image from current directory to output file with tag.

1. `docker run -p [curl-port]:[port-expose-in-dockerfile] --name [container-name] -t [image-name]:[image-tag]`
    `docker run -p 1234:80 --name i-love-go-container -t go-local:0.1`
    Docker run with its container port (1234) that linked to dockerfile exposed port (80) with the container name (origined from image name and its tag) at background.

## Publish (tag local -> push to registry)
1. `gcloud config list project` to get the project ID.
1. `docker tag [local-image]:[local-image-tag] [registry-repo]/gcloud-project-id]/[registry-image--self-defined]:[registry-image-tag--self-defined]`
    eg. `docker tag go-local:0.1 gcr.io/qwiklab-712jd0s2d03kd913j/go-pub:0.1`
1. `docker push [image-tag-with-registry]`
    eg. `docker push gcr.io/qwiklab-712jd0s2d03kd913j/go-pub:0.1`

## Common commands
`docker ps`
`docker ps -a`
`docker stop [Container-ID]`
`docker rm [Container-ID]`
`docker images`
`docker rmi [Image]`
