## Get Started
1. `docker run hello-world`
    ```
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    9db2ca6ccae0: Pull complete
    Digest: sha256:4b8ff392a12ed9ea17784bd3c9a8b1fa3299cac44aca35a85c90c5e3c7afacdc
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.
    ...
    ```
    The docker daemon searched for the hello-world image, didn't find the image locally, pulled the image from a public registry called Docker Hub, created a container from that image, and ran the container for you.

1. `docker images`
    ```
    REPOSITORY     TAG      IMAGE ID       CREATED       SIZE
    hello-world    latest   1815c82652c0   6 days ago    1.84 kB
    ```
    This is the image pulled from the Docker Hub public registry. The Image ID is in SHA256 hash format—this field specifies the Docker image that's been provisioned. When the docker daemon can't find an image locally, it will by default search the public registry for the image.

1. `docker run hello-world`
    ```
    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    ...
    ```
    Notice the second time you run this, the docker daemon finds the image in your local registry and runs the container from that image. It doesn't have to pull the image from Docker Hub.

1. `docker ps`
    Look at the running containers.

    `docker ps -a`
    Look at the all containers.

## Build
1. `mkdir test && cd test`
1. Create docker file.
    ```bash
    cat > Dockerfile <<EOF
    # Use an official Node runtime as the parent image
    FROM node:6

    # Set the working directory in the container to /app
    WORKDIR /app

    # Copy the current directory contents into the container at /app
    ADD . /app

    # Make the container's port 80 available to the outside world
    EXPOSE 80

    # Run app.js using node when the container launches
    CMD ["node", "app.js"]
    EOF
    ```
    > Read more Dockerfile command references, https://docs.docker.com/engine/reference/builder/#known-issues-run
1. Create node application.
    ```js
    cat > app.js <<EOF
    const http = require('http');

    const hostname = '0.0.0.0';
    const port = 80;

    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
            res.end('Hello World\n');
    });

    server.listen(port, hostname, () => {
        console.log('Server running at http://%s:%s/', hostname, port);
    });

    process.on('SIGINT', function() {
        console.log('Caught interrupt signal and will exit');
        process.exit();
    });
    EOF
    ```
1. `docker build -t node-app:0.1 .`
    Output:
    ```
    Sending build context to Docker daemon 3.072 kB
    Step 1 : FROM node:6
    6: Pulling from library/node
    ...
    ...
    ...
    Step 5 : CMD node app.js
    ---> Running in b677acd1edd9
    ---> f166cd2a9f10
    Removing intermediate container b677acd1edd9
    Successfully built f166cd2a9f10
    ```
    The `-t` is to name and tag an image with the name:tag syntax. The name of the image is node-app and the tag is 0.1. The tag is highly recommended when building Docker images.
    If specify a tag, the tag will default to latest and it becomes more difficult to distinguish newer images from older ones.
    Also notice how each line in the Dockerfile above results in intermediate container layers as the image is built.

## Run
1. `docker run -p 4000:80 --name my-app node-app:0.1`
    Output:
    ```
    Server running at http://0.0.0.0:80/
    ```
    The --name flag allows you to name the container if you like. The -p instructs Docker to map the host's port 4000 to the container's port 80. Now you can reach the server at http://localhost:4000. Without port mapping, you would not be able to reach the container at localhost.
    The container will run as long as the initial terminal is running.

1. Open another terminal (in Cloud Shell, click the + icon), and test the server: `curl http://localhost:4000`
    Output:
    ```
    Hello World
    ```

1. `docker stop my-app && docker rm my-app`
    Close the initial terminal and then run the following command to stop and remove the container.

1. `docker run -p 4000:80 --name my-app -d node-app:0.1`
    If you want the container to run in the background (not tied to the terminal's session), you need to specify the `-d` flag.

1. `docker logs [container_id]`


## Sample after edit file, rebuild and run
1. Edit output comment in app.js file.
1. Build `docker build -t node-app:0.2 .`
    Output:
    ```
    Step 1/5 : FROM node:6
    ---> 67ed1f028e71
    Step 2/5 : WORKDIR /app
    ---> Using cache
    ---> a39c2d73c807
    Step 3/5 : ADD . /app
    ---> a7087887091f
    Removing intermediate container 99bc0526ebb0
    Step 4/5 : EXPOSE 80
    ---> Running in 7882a1e84596
    ---> 80f5220880d9
    Removing intermediate container 7882a1e84596
    Step 5/5 : CMD node app.js
    ---> Running in f2646b475210
    ---> 5c3edbac6421
    Removing intermediate container f2646b475210
    Successfully built 5c3edbac6421
    Successfully tagged node-app:0.2
    ```
    Notice in Step 2 we are using an existing cache layer. From Step 3 and on, the layers are modified because we made a change in app.js.

1. `docker run -p 8080:80 --name my-app-2 -d node-app:0.2`
    Run another container with the new image version. Notice how we map the host's port 8080 instead of 80. We can't use host port 4000 because it's already in use.

## Debug
1. `docker logs -f [container_id]`
    Output:
    ```
    Server running at http://0.0.0.0:80/
    ```

1. `docker exec -it [container_id] bash`
    Start an interactive Bash session inside the running container.
    The -it flags let you interact with a container by allocating a pseudo-tty and keeping stdin open. Notice bash ran in the WORKDIR directory (/app) specified in the Dockerfile. From here, you have an interactive shell session inside the container to debug.

    `exit` to end the bash session.

1. `docker inspect [container_id]`
    Output:
    ```
    [
    {
        "Id": "xxxxxxxxxxxx....",
        "Created": "2017-08-07T22:57:49.261726726Z",
        "Path": "node",
        "Args": [
            "app.js"
        ],
    ...
    ```

1. `docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' [container_id]`
    Use --format to inspect specific fields from the returned JSON.
    Output:
    ```
    192.168.9.3
    ```

## Publish
Demonstrate the portability of Docker containers.
1. `gcloud config list project`
    Find project.

1. To push images to your private registry hosted by gcr (Google Container Registry)[https://cloud.google.com/container-registry/], you need to tag the images with a registry name.
    The format is `[hostname]/[project-id]/[image]:[tag]`.

    For gcr:
    [hostname]= gcr.io
    [project-id]= your project's ID
    [image]= your image name
    [tag]= any string tag of your choice. If unspecified, it defaults to "latest".

1. `docker tag node-app:0.2 gcr.io/[project-id]/node-app:0.2`

1. Push this image to gcr. Remember to replace [project-id].
    `docker push gcr.io/[project-id]/node-app:0.2`
    Output:
    ```
    The push refers to a repository [gcr.io/[project-id]/node-app]
    057029400a4a: Pushed
    342f14cb7e2b: Pushed
    903087566d45: Pushed
    99dac0782a63: Pushed
    e6695624484e: Pushed
    da59b99bbd3b: Pushed
    5616a6292c16: Pushed
    f3ed6cb59ab0: Pushed
    654f45ecb7e3: Pushed
    2c40c66f7667: Pushed
    0.2: digest: sha256:25b8ebd7820515609517ec38dbca9086e1abef3750c0d2aff7f341407c743c46 size: 2419
    ```

1. Check that the image exists in gcr by visiting the image registry in your web browser. You can navigate via the console to Tools > Container Registry or visit: http://gcr.io/[project-id]/node-app.

