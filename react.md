### Concepts
|  | | Description |
| ---- | ---- |
| `class` |  | React component class or React component type |
| `props` | Parameter | Short for preperties |
| `render()` |  | Returns 'React element', views to display via render method |
| `description` |  | What `render()` returns to display what you see on the screen |
| `state` |  | Remember things |

