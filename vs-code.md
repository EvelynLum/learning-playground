1. Disable auto break next line when single html line too long.
    - Go to `Command Palette`.
    - Type `Setting`, choose settings that wanted.
    - Type `Wrap`.
    - Choose `HTML > Format: Wrap Attribute` >> `auto`.
    - In `HTML > Format: Wrap Line Length` >> increase the number.
