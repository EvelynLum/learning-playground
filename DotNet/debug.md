`[DebuggerDisplay("{Title}")]` at the class level

data breakpoint when data changed

step into specific

In 'Locals' window, can see the
`$ReturnValue`
can call format specifiers -- Add a `,` at the end of variable

Exception snapshot in the 'Diagnostic Tools' window.

Setup break my code when exception occurs.
1. Go to 'Exception Settings' window.
1. In the list of 'Break When Thrown', check 'Common Language Runtime Exceptions'.

1. At the top menu, choose 'Debug' --> 'Other Debug Targets' --> 'Child Process Debugging Settings...'.

```c#
namespace RentGuard.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class HealthController : Controller
    {
        // GET: /<controller>/
        [HttpGet]
        public string Index()
        {
            return "OK";
        }
    }
}
```


