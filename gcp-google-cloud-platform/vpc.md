## Vocabulary
- ICMP: Internet Control Mesasge Protocol
- RTP: Remote Desktop Protocol (Port: 3389)

## Overview
1. Google Cloud Platform (GCP) Virtual Private Cloud (VPC) provides networking functionality to Compute Engine virtual machine (VM) instances, Kubernetes Engine containers and App Engine Flex.
    In other words, without a VPC network, you cannot create VM instances, containers or App Engine applications.
    Therefore, each GCP project has a default network to get you started.

1. VPC network the same way you would think of a physical network, except that it is virtualized within GCP.
    A VPC network is a global resource which consists of a list of regional virtual subnetworks (subnets) in data centers, all connected by a global wide area network (WAN).
    VPC networks are logically isolated from each other in GCP.

1. GCP firewall rules let you allow or deny traffic to and from your virtual machine (VM) instances based on a configuration.

1. Firewall rules DO NOT shared among networks.

1. In this lab, you create an auto mode VPC network with firewall rules and two VM instances. Then, you explore the connectivity for the VM instances.
    Perform the following tasks:
    - Explore the default VPC network
    - Create an auto mode network with firewall rules
    - Create VM instances using Compute Engine
    - Explore the connectivity for VM instances

## Explore the default network
1. Each GCP project has a default network with subnets, routes, and firewall rules.
1. 1 VPC network can have multiple subnets.
1. Overview:
    - View the subnets
    - View the routes
    - View the firewall rules

### View the subnets
1. Navigation menu > VPC network > VPC networks.
1. The default network has a subnet in each [GCP region](https://cloud.google.com/compute/docs/regions-zones/#available).
1. Each subnet is associated with a GCP region and a private RFC 1918 CIDR block for its internal **IP addresses range** and a **gateway**.

### View the routes
1. Navigation menu > VPC network > Routes.
1. Routes tell VM instances and the VPC network how to send traffic from an instance to a destination, either inside the network or outside of GCP.
1. Each VPC network comes with some default routes to route traffic among its subnets and send traffic from eligible instances to the Internet.
1. Notice that there is a route for each subnet and one for the **Default internet gateway** (0.0.0.0./0).
1. These routes are managed for you but you can create custom static routes to direct some packets to specific destinations. For example, you can create a route that sends all outbound traffic to an instance configured as a NAT gateway.

### View the firewall rules
1. Navigation menu > VPC network > Firewall rules.
1. Each VPC network implements a distributed virtual firewall that you can configure.
    Firewall rules allow you to control which packets are allowed to travel to which destinations.
    Every VPC network has two implied firewall rules that block all incoming connections and allow all outgoing connections.
1. The 4 Ingress firewall rules for the default network:
    - default-allow-icmp
    - default-allow-internal
    - default-allow-rdp
    - default-allow-ssh
    These firewall rules allow ICMP, RDP and SSH ingress traffic from anywhere (0.0.0.0/0) and all TCP, UDP and ICMP traffic within the network (10.128.0.0/9). The Targets, Source filters, Protocols/ports and Action columns explain these rules.
1. After default VPC network is deleted, without a VPC network, there are  no routes in 'Routes'.
1. Without a VPC network, user cannot create a VM instance with an error 'No local network available' under 'Network interface'.

## Create a VPC network with Firewall rules
1. Navigation menu > VPC network > VPC networks.
1. Create VPC network:
    Name: `mynetwork`
    Subnet creation mode: Automatic (Create subnets in each region automatically)
    Firewall rules: Check all available rules. (These are the same standard firewall rules with default network)
    > The **deny-all-ingress** and **allow-all-egress** rules are also displayed, but you cannot check or uncheck them as they are implied.
    > These two rules have a lower Priority (higher integers indicate lower priorities) so that the allow ICMP, internal, RDP and SSH rules are considered first.
1. Notice that a subnet was created for each region.
    Record the IP address range for the subnets in 'us-central1' and 'europe-west1'.
    If you ever delete the default network, you can quickly re-create it by creating an auto mode network.
1. Create a VM instance in zone `us-central1-c`.
1. Verify the **Internal IP** was assigned from the IP address range for the subnet in **us-central1** (10.128.0.0/20).
    The **Internal IP** should be 10..128.0.2 as 10.128.0.1 is reserved for the gateway and you have not configured any other insteances in that subnet.

## Explore the connectivity for VM instances
1. SSH to your VM instances using tcp:22 and ping both the internal and external IP addresses of your VM instances using ICMP.
1. Then, explore the effects of the firewall rules on connectivity by removing the firewall rules one-by-one.

### Verify connectivity for the VM instances
1. The created 'mynetwork' firewall rules:
    - allow ingress SSH and ICMP traffic
    - from within (internal IP) and outside (external IP) of the 'mynetwork' network.
1. Navigate to Navigation menu (mainmenu.png) > Compute Engine > VM instances.
1. Test 'us-central1' subnet, SSH. For 'mynet-us-vm', click SSH to launch a terminal and connect.
    You are able to SSH because of the **allow-ssh** firewall rule, which allows incoming traffic from anywhere (0.0.0.0/0) for tcp:22.
    > The SSH connection works seamlessly because Compute Engine generates an SSH key for you and stores it in one of the following locations:
    > - **By default, Compute Engine adds the generated key to project or instance metadata.**
    > - If your account is configured to use OS Login, Compute Engine stores the generated key with your user account.
    > - Alternatively, you can control access to Linux instances by creating SSH keys and editing public SSH key metadata.
1. Test 'europe-west1' subnet, ICMP-External IP. For 'mynet-eu-vm', test its external IP connectivity:
    `ping -c 3 [External IP addresses for mynet-eu-vm]`
1. Test 'europe-west1' subnet. For 'mynet-eu-vm', test its internal IP connectivity:
    `ping -c 3 [Internal IP addresses for mynet-eu-vm]`
    You are able to ping because of the **allow-internal** firewall rule.
1. Test 'europe-west1' subnet, DNS service. For 'mynet-eu-vm', ping with `mynet-eu-vm`'s name:
    `ping -c 3 mynet-eu-vm`
    You are able to ping mynet-eu-vm by its name because **VPC networks have an internal DNS service that allows you to address instances by their DNS names rather than their internal IP addresses**.
    This is very **useful as the internal IP address can change** when deleting and re-creating an instance.
    **DSN name equal to Internal IP.**
1. You were able to SSH to mynet-us-vm and ping mynet-eu-vm's internal and external IP address as expected.
    Alternatively, you could SSH to mynet-eu-vm and ping mynet-us-vm's internal and external IP address, which also works.

### Removing the allow-icmp firewall rules
1. Navigation menu > VPC network > Firewall rules.
1. Check the mynetwork-allow-icmp rule, then Delete.
1. `ping -c 3 <Enter mynet-eu-vm's internal IP here>`
    Able to ping mynet-eu-vm's internal IP because of the **allow-internal** firewall rule.
1. `ping -c 3 <Enter mynet-eu-vm's external IP here>`
    The **100% packet loss** indicates that you are unable to ping **mynet-eu-vm**'s external IP. This is expected because you deleted the **allow-icmp** firewall rule!

### Remove the allow-internal firewall rules
1. Navigation menu > VPC network > Firewall rules.
1. Check the mynetwork-allow-internal rule, then Delete.
1. `ping -c 3 <Enter mynet-eu-vm's internal IP here>`
    The **100% packet loss** indicates that you are unable to ping **mynet-eu-vm**'s internal IP. This is expected because you deleted the **allow-internal** firewall rule!

### Remove the allow-ssh firewall rules
1. Navigation menu > VPC network > Firewall rules.
1. Check the mynetwork-allow-ssh rule, then Delete.
1. Navigate to the vm instance and relaunch SSH terminal:
    The **Connection failed** message indicates that you are unable to SSH to **mynet-us-vm** because you deleted the **allow-ssh** firewall rule!
