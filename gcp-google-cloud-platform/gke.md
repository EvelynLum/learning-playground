## General steps
1. Setup default compute zone -> Create Kubernetes Engine cluster -> Get access authentication credential -> Deploy containerized application to the cluster -> Clean up (Optional)

# Google Kubernetes Engine (GKE)
1. GKE provides a managed environment for deploying, managing, and scaling your containerized applications using Google infrastructure.
1. The Kubernetes Engine environment consists of multiple machines (specifically Google Compute Engine instances) grouped together to form a [container cluster](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-architecture).
1. Kubernetes design principles:
    - automatic management
    - monitoring
    - liveness probes for application containers, automatic scaling, rolling updates, and more.
1. Kubernetes Engine cluster on GCP, you gain the benefit of advanced cluster management features:
    - [Load-balancing](https://cloud.google.com/compute/docs/load-balancing-and-autoscaling) for Compute Engine instances.
    - [Node Pools](https://cloud.google.com/kubernetes-engine/docs/node-pools) to designate subsets of nodes within a cluster for additional flexibility.
    - [Automatic scaling](https://cloud.google.com/kubernetes-engine/docs/cluster-autoscaler) of your cluster's node instance count.
    - [Automatic upgrades](https://cloud.google.com/kubernetes-engine/docs/node-auto-upgrade) for your cluster's node software.
    - [Node auto-repair](https://cloud.google.com/kubernetes-engine/docs/how-to/node-auto-repair) to maintain node health and availability.
    - [Logging and Monitoring](https://cloud.google.com/kubernetes-engine/docs/how-to/logging) with Cloud Monitoring for visibility into your cluster.

## Creating a Kubernetes Engine cluster
1. A [cluster](https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-architecture) consists of at least one cluster master machine and multiple worker machines called nodes.
    Nodes are Compute Engine virtual machine (VM) instances that run the Kubernetes processes necessary to make them part of the cluster.
1. `gcloud container clusters create [CLUSTER-NAME]`
    Createing a Kubernetes Engine cluster.
1. `gcloud container clusters get-credentials [CLUSTER-NAME]`
    Get authentication credentials to interact with the cluster.
1. After cluster created, it is ready to deploy [containerized application](https://cloud.google.com/kubernetes-engine/docs/concepts/kubernetes-engine-overview).
1. `kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0`
    Create 'hello-server' deployment server from the 'hello-app' container image in Google Container Registry.
1. Kubernetes Engine uses Kubernetes objects to create and manage your cluster's resources.
    - [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) object for deploying stateless applications like web servers.
    - [Service](https://kubernetes.io/docs/concepts/services-networking/service/) objects define rules and load balancing for accessing your application from the Internet.
1. `kubectl expose deployment hello-server --type=LoadBalancer --port 8080`
    Create a Kubernetes Service (a Kubernetes resource) that expose the application to external traffic.
    - `--port` specifies the port that the container exposes.
    - `type="LoadBalancer"` creates a Compute Engine load balancer for your container.
    - Output:
        `service/hello-server exposed`
1. `kubectl get service`
    Inspect the 'hello-server' Service.
    - It might take a minute for an external IP address to be generated. Run `kubectl get service` to check 'EXTERNAL-IP' status. Re-run again if the `EXTERNAL-IP` column is in "pending" status.
1. `http://[EXTERNAL-IP]:8080`
    - View application in the browser with the external IP address with the exposed port.
1. `gcloud container clusters delete [CLUSTER-NAME]`
    Delete the cluster. Might take a few minutes.


