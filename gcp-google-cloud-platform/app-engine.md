## Overview
1. App Engine allows developers to focus on doing what they do best, writing code.
1. The App Engine standard environment is based on container instances running on Google's infrastructure.
1. Containers are preconfigured with one of several available runtimes (Java 7, Java 8, Python 2.7, Go and PHP).
    - Each runtime also includes libraries that support App Engine Standard APIs.
    - For many applications, the standard environment runtimes and libraries might be all you need.

### Go language environments
1. App Engine offers you two Go language environments:
    - App Engine standard environment
    - App Engine flexible environment
1. Both environments common in:
    - same code-centric developer workflow
    - scale quickly and efficiently to handle increasing demand
    - enable you to use Google’s proven serving technology to build your web, mobile and IoT applications quickly with minimal operational overhead.
1.  Different in:
    | App Engine standard environment | App Engine flexible environment |
    |----|----|
    | Easy to build  | Based on Google Compute Engine |
    | Easy to deploy an application that runs reliably under heavy load and with large amounts of data. | Automatically scales your app up and down while balancing the load |
    | The application runs within its own secure, reliable environment that is independent of the hardware, operating system, or physical location of the server. |  |
    | Supports Go 1.11. |  |

### Enable Google App Engine Admin API
1. The App Engine Admin API enables developers to provision 规定 and manage their App Engine Applications.
1. Steps:
    - Menu > APIs & Services > Library.
    - Type "App Engine Admin API" in search box.
    - Click "App Engine Admin API".
    - Click Enable.

### Download, Deploy, Test
1. `git clone https://github.com/GoogleCloudPlatform/golang-samples.git`
1. `cd golang-samples/appengine/go11x/helloworld`
1. In the root directory of your application where the app.yaml file is located, run:
    ```shell
    gcloud components install app-engine-go
    gcloud app deploy
    ```
1. `gcloud app browse`
