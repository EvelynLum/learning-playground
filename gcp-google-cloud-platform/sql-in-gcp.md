# Table of Contents
- [Google Cloud Data Store (NoSQL)](#Google-Cloud-Data-Store)
- [Cloud SQL](#Cloud-SQL)

# Google Cloud Data Store
## Overview
1. Google Cloud Datastore is a NoSQL document database built for automatic scaling, high performance, and ease of application development.
1. Cloud Datastore supports querying data by
    - Query by Kind
    - Google Query Language (GQL)

### Store Data
1. Datastore > Entities.
1. Under the Datastore mode column, click 'Select Datastore Mode'.
1. Choose location with the dropdown menu.
    The location applies to both Cloud Datastore and App Engine for your Google Cloud Platform project. You cannot change the location after it has been saved.
1. Click 'Create database'.
1. Click 'Create Entity'.
1. On the Create an entity page, use [default] for Namespace.
1. Type `Task` for 'Kind'.
1. Under 'Properties' use the 'Add property' button to add these properties, and click 'Done' after each one:
    | Name | Type | Value | Indexed |
    |----|----|----|----|
    | description | String | Learn Google Cloud Datastore | ✕ |
    | created | Date and time | (today's date) | ✓ |
    | done | Boolean | False | ✓ |
1. Click Create. The console displays the Task entity that you just created.

### Run a query (Query by Kind)
1. Datastore > Entities.
1. Click 'Query by kind' tab.
1. Add a query filter to restrict the results to entities that meet specific criteria:
    - Click 'Filter entities' tab.
    - In the dropdown lists, select 'done', 'is a boolean', and 'that is false'.
    - Click 'Apply filters'.
    - The results show the Task entity that you created, since its done value is false.
    <br/><br/>
    - Try a query of 'done', 'is a boolean', and 'that is true' then 'Apply filters'.
        The results do not include the Task entity that you created, because its done value is not true.

### Run a query (Query by GQL)
1. Datastore > Entities.
1. Click the 'Query by GQL' tab.
1. In the query box add the following:
    `SELECT * FROM Task WHERE done=false`
1. Note that `Task` (entity name) and `done` (value) are **case sensitive**.
1. Autocompletion for kinds is supported: Press **Ctrl+Space** to see a list of the available kinds.
    Up to 300 alphabetically sorted kinds can appear in the list.

# Cloud SQL
## Create a Cloud SQL instance
1. Menu > SQL
1. Create database instance of your choice.
    Instance ID is used to uniquely identify your instance within the project.
## Connect to your instance using the mysql client in the Cloud Shell
1. Click the 'Cloud Shell'.
1. `gcloud sql connect [Instance Name] --user=root`
    - `mysql` prompt will be shown when successfully connected.
1. Create db, insert, select as usual db operations.

# Data Lose Prevention
## Overview
1. The [Data Loss Prevention](https://cloud.google.com/dlp/) API provides programmatic access to a powerful detection engine for personally identifiable information (PII) and other privacy-sensitive data in unstructured data streams.
    eg. Input: `node inspect.js string "My email address is joe@example.com."`
    Output:
    ```
    Findings:
       Quote: joe@example.com
       Info type: EMAIL_ADDRESS
       Likelihood: LIKELY
    ```

1. The DLP API provides fast, scalable classification and optional redaction 编辑 for sensitive data elements like credit card numbers, names, social security numbers, passport numbers, and phone numbers.
1. The API supports text and images – just send data to the API or specify data stored on your Google Cloud Storage, BigQuery, and Cloud Datastore instances.

1. Use the Data Loss Prevention API to inspect a string of data for sensitive information.
    Demo Steps:
    - `git clone https://github.com/googleapis/nodejs-dlp.git`
    - `export GCLOUD_PROJECT=[YOUR_PROJECT_ID]`
    - Install dependecies:
        ```
        npm install --save @google-cloud/dlp
        npm install yargs
        ```
    - `cd nodejs-dlp/samples`
    - Inspect a string for sensitive information.
        Sample Positive: `node inspect.js string "My email address is joe@example.com."`
        Output:
        ```
        Findings:
            Quote: joe@example.com
            Info type: EMAIL_ADDRESS
            Likelihood: LIKELY
        ```
        Sample Negative: `node inspect.js string "My address is +6012-823 5555"`
        Output:
        ```
        Findings:
            Quote: +6012-823 5555
            Info type: PHONE_NUMBER
            Likelihood: POSSIBLE
        ```
        _It is able to recognized the input value possible to match with 'Phone_Number' as info type than address._
1. The result shows:
    - what the piece of sensitive data was found
    - what type of information it is
    - how sure the API is that the string contains sensitive information











