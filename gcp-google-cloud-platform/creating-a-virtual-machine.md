## Create a new instances from the Cloud Console
1. Parameters to create new instances:
    | Field | Value | Additional Information |
    |----|----|----|
    | Name | `gcelab` | Name for the VM instance |
    | Region | us-central1 (Iowa) or asia-south1 (Mumbai) |  |
    | Zone | us-central1c or asia-south1c |  |
    | Machine Type | 2 vCPUs (n1-standard-2), 2-CPU, 7.5GB RAM instance. [Machine Types documentation](https://cloud.google.com/compute/docs/machine-types) | Processor related. Choose number of CPU cores. [Resource quota](https://cloud.google.com/compute/docs/resource-quotas) |
    | Boot Disk | New 10 GB standard persistent disk. <br/> OS Image: Debian GNU/Linux 9 (stretch) | Image related, choose OS. |
    | Firewall | Check 'Allow HTTP traffic' | This will automatically create firewall rule to allow HTTP traffic on port 80. |

1. Create VM instances at Compute Engine.
1. After VM instance is created, it will show at **VM Instances** page.
1. To SSH into the virtual machine, click the **SSH** dropdown on the right.
[Reference: Connect to an instance using ssh](https://cloud.google.com/compute/docs/instances/connecting-to-instance)

## Install a NGINX web server
1. After ssh, get root access using sudo:
    `sudo su -`
1. `apt-get update` update OS.
1. `apt-get install nginx -y`
1. `ps auwx | grep nginx`
1. Awesome! To see the web page, go to the Cloud Console and click the `External IP` link of the virtual machine instance. You can also see the web page by adding the `External IP` to `http://EXTERNAL_IP/` in a new browser window or tab.

## Create a new instance with gcloud
1. `gcloud compute instances create gcelab2 --machine-type n1-standard-2 --zone [your_zone]`
1. `gcloud compute instances create --help`
1. *Note:* If you are always working within one region/zone, set the default region and zones that gcloud uses, no need to append the --zone flag every time.
    `gcloud config set compute/zone ...`
    `gcloud config set compute/region ...`
1. `gcloud compute ssh gcelab2 --zone [YOUR_ZONE]` `--zone [YOUR_ZONE]` is optional if gobally has been set.
