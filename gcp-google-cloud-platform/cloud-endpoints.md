### Deploying the Endpoints Configuration
1. To publish a REST API to Endpoints, an OpenAPI configuration file that describes the API is required.
    The lab comes with a pre-configured OpenAPI file called `openapi.yaml`.

1. Endpoints uses Google `Service Management`, an infrastructure service of GCP, to create and manage APIs and services.
    To use Endpoints to manage an API, you deploy the API's OpenAPI configuration to Service Management.

1. To deploy the Endpoints configuration.
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    main() {
    # Get our working project, or exit if it's not set.
    local project_id=$(get_project_id)
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    local temp_file=$(mktemp)
    export TEMP_FILE="${temp_file}.yaml"
    mv "$temp_file" "$TEMP_FILE"
    # Because the included API is a template, we have to do some string
    # substitution before we can deploy it. Sed does this nicely.
    < "$API_FILE" sed -E "s/YOUR-PROJECT-ID/${project_id}/g" > "$TEMP_FILE"
    echo "Deploying $API_FILE..."
    echo "gcloud endpoints services deploy $API_FILE"
    gcloud endpoints services deploy "$TEMP_FILE"
    }

    cleanup() {
    rm "$TEMP_FILE"
    }

    # Defaults.
    API_FILE="../openapi.yaml"

    if [[ "$#" == 0 ]]; then
    : # Use defaults.
    elif [[ "$#" == 1 ]]; then
    API_FILE="$1"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: deploy_api.sh [api-file]"
    exit 1
    fi

    # Cleanup our temporary files even if our deployment fails.
    trap cleanup EXIT

    main "$@"
    ```

1. Cloud Endpoints uses the `host` field in the OpenAPI configuration file to identify the service.
1. The `deploy_api.sh` script sets the ID of your Cloud project as part of the name configured in the host field.
    (When you prepare an OpenAPI configuration file for your own service, you will need to do this manually.)

1. The script then deploys the OpenAPI configuration to Service Management using the command: `gcloud endpoints services deploy openapi.yaml`

1. As it is creating and configuring the service, Service Management outputs some information to the console.
1. You can safely ignore the warnings about the paths in openapi.yaml not requiring an API key.
    On successful completion, you see a line like the following that displays the service configuration ID and the service name:
    ```
    Service Configuration [2017-02-13-r2] uploaded for service [airports-api.endpoints.example-project.cloud.goog]
    ```


### Deploying the API backend
1. `./deploy_app.sh`
- The script create an App Engine flexible environment in the us-central region: `gcloud app create --region="$REGION"`.
- `gcloud app deploy` command to deploy the sample API to App Engine.
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    main() {
    # Get our working project, or exit if it's not set.
    local project_id="$(get_project_id)"
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    # Try to create an App Engine project in our selected region.
    # If it already exists, return a success ("|| true").
    echo "gcloud app create --region=$REGION"
    gcloud app create --region="$REGION" || true
    # Prepare the necessary variables for substitution in our app configuration
    # template, and create a temporary file to hold the templatized version.
    local service_name="${project_id}.appspot.com"
    export TEMP_FILE="../app/app.yaml"
    < "$APP" \
        sed -E "s/SERVICE_NAME/${service_name}/g" \
        > "$TEMP_FILE"
    echo "Deploying ${APP}..."
    echo "gcloud -q app deploy $TEMP_FILE"
    gcloud -q app deploy "$TEMP_FILE"
    }

    cleanup() {
    rm "$TEMP_FILE"
    }

    # Defaults.
    APP="../app/app_template.yaml"
    REGION="us-central"

    if [[ "$#" == 0 ]]; then
    : # Use defaults.
    elif [[ "$#" == 1 ]]; then
    APP="$1"
    elif [[ "$#" == 2 ]]; then
    APP="$1"
    REGION="$2"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: deploy_app.sh [app-template] [region]"
    exit 1
    fi

    # Cleanup our temporary files even if our deployment fails.
    trap cleanup EXIT

    main "$@"
    ```


### Tracking API activity
- Populate the graphs and logs: `./generate_traffic.sh`
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    # Use this to keep track of what HTTP status codes we receive.
    declare -A codes

    # generate_traffic will print a status update every UPDATE_FREQUENCY messages.
    UPDATE_FREQUENCY=25

    main() {
    # Get our working project, or exit if it's not set.
    local project_id=$(get_project_id)
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    export url="https://${project_id}.appspot.com/airportName?iataCode=${IATA_CODE}"
    echo "This command will exit automatically in $TIMEOUT_SECONDS seconds."
    echo "Generating traffic to ${url}..."
    echo "Press Ctrl-C to stop."
    local endtime=$(($(date +%s) + $TIMEOUT_SECONDS))
    local request_count=0
    # Send queries repeatedly until TIMEOUT_SECONDS seconds have elapsed.
    while [[ $(date +%s) -lt $endtime ]]; do
        request_count=$(( request_count + 1))
        if [[ $((request_count % UPDATE_FREQUENCY)) == 0 ]]; then
        echo "Served ${request_count} requests."
        fi
        # Make the HTTP request and save its status in an associative array.
        http_status=$(curl -so /dev/null -w "%{http_code}" "$url")
        if [[ "${!codes[@]}" != *"$http_status"* ]]; then
        codes["$http_status"]="1"
        else
        codes["$http_status"]="$(( ${codes[$http_status]} + 1 ))"
        fi
    done
    }

    print_status() {
    echo ""
    echo "HTTP status codes received from ${url}:"
    for code in "${!codes[@]}"; do
        echo "${code}: ${codes[$code]}"
    done
    }

    # Defaults.
    IATA_CODE="SFO"
    TIMEOUT_SECONDS=$((5 * 60)) # Timeout after 5 minutes.

    if [[ "$#" == 0 ]]; then
    : # Use defaults.
    elif [[ "$#" == 1 ]]; then
    IATA_CODE="$1"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: generate_traffic.sh [iata-code]"
    exit 1
    fi

    # Print the received codes when we exit.
    trap print_status EXIT

    main "$@"
    ```

### Add a quota to the API
1. Deploy the Endpoints configuration that has a quota:
    `./deploy_api.sh ../openapi_with_ratelimit.yaml`
1. Redeploy your app to use the new Endpoints configuration (this may take a few minutes):
    `./deploy_app.sh`
1. Navigate to Navigation menu > API & Services > Credentials.
1. Click Create credentials and choose API key. A new API key is displayed on the screen, copy it to your clipboard.
1. In Cloud Shell, type `export API_KEY={COPIED-API-KEY}`
1. `./query_api_with_key.sh $API_KEY`
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    main() {
    # Get our working project, or exit if it's not set.
    local project_id=$(get_project_id)
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    # Because our included app uses query string parameters, we can include
    # them directly in the URL. We use -H to specify a header with our API key.
    QUERY="curl -H 'x-api-key: $API_KEY' \"https://${project_id}.appspot.com/airportName?iataCode=${IATA_CODE}\""
    # First, print the command so the user can see what's being executed.
    echo "$QUERY"
    # Then actually execute it.
    # shellcheck disable=SC2086
    eval $QUERY
    # Our API doesn't print newlines. So we do it ourselves.
    printf '\n'
    }

    # Defaults.
    IATA_CODE="SFO"

    if [[ "$#" == 1 ]]; then
    API_KEY="$1"
    elif [[ "$#" == 2 ]]; then
    # "Quiet mode" won't print the curl command.
    API_KEY="$1"
    IATA_CODE="$2"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: query_api_with_key.sh api-key [iata-code]"
    exit 1
    fi

    main "$@"
    ```
1. `./query_api_with_key.sh $API_KEY` Send your API a request using the API key variable you just created
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    main() {
    # Get our working project, or exit if it's not set.
    local project_id=$(get_project_id)
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    # Because our included app uses query string parameters, we can include
    # them directly in the URL. We use -H to specify a header with our API key.
    QUERY="curl -H 'x-api-key: $API_KEY' \"https://${project_id}.appspot.com/airportName?iataCode=${IATA_CODE}\""
    # First, print the command so the user can see what's being executed.
    echo "$QUERY"
    # Then actually execute it.
    # shellcheck disable=SC2086
    eval $QUERY
    # Our API doesn't print newlines. So we do it ourselves.
    printf '\n'
    }

    # Defaults.
    IATA_CODE="SFO"

    if [[ "$#" == 1 ]]; then
    API_KEY="$1"
    elif [[ "$#" == 2 ]]; then
    # "Quiet mode" won't print the curl command.
    API_KEY="$1"
    IATA_CODE="$2"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: query_api_with_key.sh api-key [iata-code]"
    exit 1
    fi

    main "$@"
    ```
1. You'll see something like the following on the console:
    ```
    curl -H 'x-api-key: AIzeSyDbdQdaSdhPMdiAuddd_FALbY7JevoMzAB' "https://example-project.appspot.com/airportName?iataCode=SFO"
    San Francisco International Airport
    ```
1. The API now has a limit of 5 requests per second. Send traffic to the API and trigger the quota limit: `./generate_traffic_with_key.sh $API_KEY`
    ```sh
    #!/bin/bash
    set -euo pipefail

    source util.sh

    # Use this to keep track of what HTTP status codes we receive.
    declare -A codes

    # generate_traffic will print a status update every UPDATE_FREQUENCY messages.
    UPDATE_FREQUENCY=25

    main() {
    # Get our working project, or exit if it's not set.
    local project_id=$(get_project_id)
    if [[ -z "$project_id" ]]; then
        exit 1
    fi
    export url="https://${project_id}.appspot.com/airportName?iataCode=${IATA_CODE}&key=${API_KEY}"
    echo "This command will exit automatically in $TIMEOUT_SECONDS seconds."
    echo "Generating traffic to ${url}..."
    echo "Press Ctrl-C to stop."
    local endtime=$(($(date +%s) + $TIMEOUT_SECONDS))
    local request_count=0
    # Send queries repeatedly until TIMEOUT_SECONDS seconds have elapsed.
    while [[ $(date +%s) -lt $endtime ]]; do
        request_count=$(( request_count + 1))
        if [[ $((request_count % UPDATE_FREQUENCY)) == 0 ]]; then
        echo "Served ${request_count} requests."
        fi
        # Make the HTTP request and save its status in an associative array.
        http_status=$(curl -so /dev/null -w "%{http_code}" "$url")
        if [[ "${!codes[@]}" != *"$http_status"* ]]; then
        codes["$http_status"]="1"
        else
        codes["$http_status"]="$(( ${codes[$http_status]} + 1 ))"
        fi
    done
    }

    print_status() {
    echo ""
    echo "HTTP status codes received from ${url}:"
    for code in "${!codes[@]}"; do
        echo "${code}: ${codes[$code]}"
    done
    }

    # Defaults.
    IATA_CODE="SFO"
    TIMEOUT_SECONDS=$((5 * 60)) # Timeout after 5 minutes.

    if [[ "$#" == 1 ]]; then
    API_KEY="$1"
    elif [[ "$#" == 2 ]]; then
    API_KEY="$1"
    IATA_CODE="$2"
    else
    echo "Wrong number of arguments specified."
    echo "Usage: generate_traffic_with_key.sh api-key [iata-code]"
    exit 1
    fi

    # Print the received codes when we exit.
    trap print_status EXIT

    main "$@"
    ```
1. You'll see something like the following on the console:
    ```
    {
     "code": 8,
     "message": "Insufficient tokens for quota 'airport_requests' and limit 'limit-on-airport-requests' of service 'example-project.appspot.com' for consumer 'api_key:AIzeSyDbdQdaSdhPMdiAuddd_FALbY7JevoMzAB'.",
     "details": [
      {
       "@type": "type.googleapis.com/google.rpc.DebugInfo",
       "stackEntries": [],
       "detail": "internal"
      }
     ]
    }
    ```
If you get a different response, try running the `generate_traffic_with_key.sh` script again and retry.
