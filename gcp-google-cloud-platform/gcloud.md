`gcloud` -> Cloud SDK

## Common commands
- `gcloud init`
- `gcloud info`
- `gcloud auth list`
- `compute-default-region`
- `google-compute-default-zone`
- `gcloud config --help` OR `gcloud help config`
- `gcloud config list`, `gcloud config list --all`
- `gcloud config list project`
- `gcloud components list`
- `gcloud compute project-info describe --project <your_project_ID>`
- `gcloud compute images list`
- `gcloud compute instances describe <your_vm>`
- `gcloud compute firewall-rules create [Rule-Name] --allow tcp:80 --source-ranges 0.0.0.0/0 --network default`
-
-

## Understanding Regions and Zones
Region: `us-central1`; Zones: `us-central1-a`, `us-central1-b`, `us-central1-c`, `us-central1-f`

|Western US|Central US|
|----|----|
|us-west1-a|us-central1-a|
|us-west1-b|us-central1-b|
||us-central1-c|
||us-central1-f|

Resources that live in a zone
- Zonal resources.
- Virtual machine Instances and persistent disks live in a zone.
- To attach a persistent disk to a virtual machine instance, both resources must be in the same zone.
- Similarly, to assign a static IP address to an instance, the instance must be in the same region as the static IP.

[Reference: Regions and Zones](https://cloud.google.com/compute/docs/regions-zones/)

## Update gcloud in CentOS-7
1. ```shell
    # Update YUM with Cloud SDK repo information:
    sudo tee -a /etc/yum.repos.d/google-cloud-sdk.repo << EOM
    [google-cloud-sdk]
    name=Google Cloud SDK
    baseurl=https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64
    enabled=1
    gpgcheck=1
    repo_gpgcheck=1
    gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
        https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
    EOM

    # The indentation for the 2nd line of gpgkey is important.

    # Install the Cloud SDK
    sudo yum install google-cloud-sdk -y
    ```

## Init and Create Virtual Machine
1. `gcloud init`
    Initializing Cloud SDK
1. `gcloud compute instances create gcelab2 --machine-type n1-standard-2 --zone $ZONE`
1. `gcloud compute instances create --help`

### Create instance with Container-Optimized OS images
1. List available Container-Optimized OS images:
    ```shell
    gcloud compute images list \
    --project cos-cloud \
    --no-standard-images
    ```
1. Use the gcloud compute instances create command with --image and --image-project flags to create a cos node image instance:
    ```
     gcloud beta compute instances create-with-container containerized-vm2 \
     --image cos-stable-72-11316-136-0 \
     --image-project cos-cloud \
     --container-image nginx \
     --container-restart-policy always \
     --zone us-central1-a \
     --machine-type n1-standard-1
    ```
    - `cos-stable-72-11316-136-0` is one of the available cos releases.
    - Use the latest available image from `cos-stable` family and replace it with an image for your VM instance.
    - Use `--preemptible` flag for one-off experimental instances.
1. Create firewall rules to allow HTTP traffic from the internet and to enable all internal traffic within the VPC:
    ```
    gcloud compute firewall-rules create allow-containerized-internal\
    --allow tcp:80 \
    --source-ranges 0.0.0.0/0 \
    --network default
    ```

## Authorizing the SDK tools to access GCP with your User Account Credentials
1. `gcloud init --console-only`
    This prevents the `gcloud init` from launching a web browser.
1. Choose option `2`, to log in with a new account.
1. You will get confirmation that you're running on virtual machine.
    Type `Y` to allow the credentials you logged into the lab with (this is your personal account for this lab) to be used to authenticate your account.
1. A long URL click on it or paste it into a new browser.
1. Select your lab credentials again, and 'Allow' access to your account.
1. This URL will give you your authentication code. Copy the code and paste it into the SSH window, then press Enter.
1. In SSH window, type the number corresponding to you GCP Project ID.
1. Success output:
    ```
    Your current project has been set to [qwiklabs-gcp-fe1e6438a8b814c2].
    ...

    This gcloud configuration is called [default]
    ```

## Auto-completion
1. `gcloud interactive`
    General term.
1. `gcloud components install beta`
    Install auto-completion component.
1. `gcloud beta interactive`
    Enter `gcloud interactive` mode.
1. F2:help:STATE Toggles the active help section, ON when enabled, OFF when disabled.

## SSH into your vm instance
1. `gcloud compute`
    General term. Google Compute Engine resources in a friendlier format than using the Compute Engine API.
1. `gcloud compute ssh gcelab2 --zone $ZONE`
    The gcloud compute ssh command provides a wrapper around SSH, which takes care of authentication and the mapping of instance name to IP address.
1. `exit`
    Terminate the ssh session.

## Create a service account key
1. `export PROJECT_ID=[YOUR_PROJECT_ID]`
1. Create a Service Account to access the Google Cloud APIs when testing locally:
    `gcloud iam service-accounts create qwiklab --display-name "Qwiklab Service Account"`
1. Give your newly created Service Account appropriate permissions:
    ```
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:qwiklab@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/owner
    ```
1. Create a Service Account key for created service account above:
    ```
    gcloud iam service-accounts keys create ~/key.json \
    --iam-account qwiklab@${PROJECT_ID}.iam.gserviceaccount.com
    ```
    Output: A service account key (long string) generate.
1. Store the service account key in a JSON file named 'key.json' in your home directory.
1. Authenticate to your service account, passing the location of your service account key file:
    `gcloud auth activate-service-account --key-file=key.json`
1. Obtain an authorization token using your service account:
    `gcloud auth print-access-token`


