## Overview
1. Container-Optimized OS is an operating system image for your Compute Engine VMs
1. The image is:
    - optimized for running Docker containers
    - Google's recommended OS for running containers on GCP.

1. Since the image comes with all preinstalled container-related dependencies, Container-Optimized OS allows your cluster:
    - to quickly scale up or down in response to traffic or workload changes
    - optimizing your spend
    - improving your reliability.

1. Container-Optimized OS powers many GCP services such as Kubernetes Engine and Cloud SQL, making it Google's go-to solution for container workloads.

1. Container-Optimized OS benefits
    - `Run Containers Out of the Box`: Container-Optimized OS instances come pre-installed with the Docker runtime and cloud-init. With a Container-Optimized OS instance, you can bring up your Docker container at the same time you create your VM, with no on-host setup required.
    - `Smaller attack surface`: Container-Optimized OS has a smaller footprint, reducing your instance's potential attack surface.
    - `Locked-down by default`: Container-Optimized OS instances include a locked-down firewall and other security settings by default.
    - `Automatic Updates`: Container-Optimized OS instances are configured to automatically download weekly updates in the background; only a reboot is necessary to use the latest updates.

1. Use cases for Container-Optimized OS:
    Container-Optimized OS can be used to run most Docker containers.
    You should consider Container-Optimized OS as your Compute Engine instance's OS when:
    - You need support for Docker containers or Kubernetes with minimal setup.
    - You need an operating system that has a small footprint and is security hardened for containers.
    - You need an operating system that is tested and verified for running Kubernetes on your Compute Engine instances.

1. Container-Optimized OS features:
    - Compute Engine provides several [public VM images](https://cloud.google.com/compute/docs/images#os-compute-support) that you can use to create instances and run your container workloads.
    - Some of these public VM images have a minimalistic container-optimized operating system that includes newer versions of Docker, rkt, or Kubernetes preinstalled.
1. Public image families are designed specifically to run containers:
    | VM Image | Includes | Image Project | Image Family |
    |----|----|----|----|
    | [Container-Optimized OS from Google](https://cloud.google.com/container-optimized-os/docs/) | Docker, Kubernetes | cos-cloud | cos-stable |
    | [CoreOS](https://coreos.com/) | Docker, rkt, Kubernetes | coreos-cloud | coreos-stable |
    | [Ubuntu](https://www.ubuntu.com/) | LXD | ubuntu-os-cloud | ubuntu-1604-lts |
    | [Windows](https://www.microsoft.com/) | Docker | windows-cloud | windows-1709-core-for-containers |

1. If specific container tools and technologies do not included in the image by default, [install](https://cloud.google.com/compute/docs/containers/#installing) those technologies manually.

### Create instance with CLI
1. List available Container-Optimized OS images:
    ```shell
    gcloud compute images list \
    --project cos-cloud \
    --no-standard-images
    ```
1. Use the gcloud compute instances create command with --image and --image-project flags to create a cos node image instance:
    ```
     gcloud beta compute instances create-with-container containerized-vm2 \
     --image cos-stable-72-11316-136-0 \
     --image-project cos-cloud \
     --container-image nginx \
     --container-restart-policy always \
     --zone us-central1-a \
     --machine-type n1-standard-1
    ```
    - `cos-stable-72-11316-136-0` is one of the available cos releases.
    - Use the latest available image from `cos-stable` family and replace it with an image for your VM instance.
    - Use `--preemptible` flag for one-off experimental instances.
1. Create firewall rules to allow HTTP traffic from the internet and to enable all internal traffic within the VPC:
    ```
    gcloud compute firewall-rules create allow-containerized-internal\
    --allow tcp:80 \
    --source-ranges 0.0.0.0/0 \
    --network default
    ```
