## GCP Projects
1. GCP Project - An organizing entity for anything you build with the Google Cloud Platform.
1. It's common for large enterprises or experienced users of GCP to have dozens to thousands of GCP projects. Organizations use GCP in different ways, so projects are a good way to separate cloud-computing services (by team or product for example.)
1. Qwiklabs Resources is not the project where you run all of your lab steps. <br/>
    "Qwiklabs Resources" is a project that contains files, datasets, and machine images for certain labs and can be accessed from every GCP lab environment. It's important to note that "Qwiklabs Resources" is shared (read only) with all Qwiklabs users, meaning you won't be able to delete or modify it.
    <br/>
    The GCP project that you are working out of and whose name resembles qwiklabs-gcp-xxx... is temporary, meaning the project and everything it contains will be deleted once the lab ends. Whenever you start a new lab, you will be given access to one or more new GCP project(s), and there (not "Qwiklabs Resources") is where you will run all of the lab steps.

## GCP Services
1. 7 categories of GCP services:
    | Compute | houses a variety of machine types that support any type of workload. The different computing options let you decide how involved you want to be  with operational details and infrastructure amongst other things. |
    | Storage | data storage and database options for structured or unstructured, relational or non relational data. |
    | Networking | services that balance application traffic and provision security rules amongst other things. |
    | Stackdriver | a suite of cross-cloud logging, monitoring, trace, and other service reliability tools. |
    | Tools | services for developers managing deployments and application build pipelines. |
    | Big Data | services that allow you to process and analyze large datasets. |
    | Artificial Intelligence | a suite of APIs that run specific artificial intelligence and machine learning tasks on the Google Cloud platform. |

## Roles and Permisssions
1. Open up the navigation menu, near the top click 'IAM & admin'. This page contains a list of users, which specifies permissions and roles granted to certain accounts.
1. Reference: [Cloud Identity and Access Management (IAM)](https://cloud.google.com/iam/)
1. Primitive roles set project-level permissions and unless otherwise specified, they control access and management to all GCP services.

    | Role Name | Permissions |
    |----|----|
    | roles/viewer | Permissions for read-only actions that do not affect state, such as viewing (but not modifying) existing resources or data. |
    | roles/editor | All viewer permissions, plus permissions for actions that modify state, such as changing existing resources. |
    | roles/owner | All editor permissions and permissions for the following actions: <br/>Manage roles and permissions for a project and all resources within the project.<br/>Set up billing for a project. |

## APIs and Services
1. Cloud APIs use resource-oriented design principles as described in our [Google API Design Guide](https://cloud.google.com/apis/design/)).
1. Most Cloud APIs provide you with detailed information on your project’s usage of that API, including traffic levels, error rates, and even latencies, helping you to quickly triage problems with applications that use Google services.
1. The Dialogflow API allows you to build rich conversational applications (e.g. for Google Assistant) without having to worry about the underlying machine learning and natural language understanding schema.

