# Load Balancer
1. [Load Balance Overview](https://cloud.google.com/load-balancing/docs/load-balancing-overview#a_closer_look_at_cloud_load_balancers)
1. 2 types of load balancer
    | Type | |  |
    |----|----|----|
    | Network load balancer | L3 | Network Load Balancing is a regional, non-proxied load balancer. |
    | HTTP(S) load balancer | L7 |  |

## Create multiple web service instances
1. To simulate serving from a cluster of machines, create a simple cluster of Nginx web servers to serve static content using Instance Templates and Managed Instance Groups.
    - [Instance Templates](https://cloud.google.com/compute/docs/instance-templates) define the look of every virtual machine in the cluster (disk, CPUs, memory, etc).
    - [Managed Instance Groups](https://cloud.google.com/compute/docs/instance-groups/) instantiate a number of virtual machine instances using the Instance Template.
1. To create the Nginx web server clusters, create the following:
    - A startup script to be used by every virtual machine instance to setup Nginx server upon startup
    - An instance template to use the startup script
    - A target pool
    - A managed instance group using the instance template
1. Create a startup script to be used by every virtual machine instance. This script sets up the Nginx server upon startup:
    ```shell
    cat << EOF > startup.sh
    #! /bin/bash
    apt-get update
    apt-get install -y nginx
    service nginx start
    sed -i -- 's/nginx/Google Cloud Platform - '"\$HOSTNAME"'/' /var/www/html/index.nginx-debian.html
    EOF
    ```
1. `gcloud compute instance-templates create nginx-template \
    --metadata-from-file startup-script=startup.sh`
    Create an instance template, which uses the startup script.
1. `gcloud compute target-pools create nginx-pool`
    Create a target pool. A target pool allows a single access point to all the instances in a group and is necessary for load balancing in the future steps.
1. ```shell
    gcloud compute instance-groups managed create nginx-group \
         --base-instance-name nginx \
         --size 2 \
         --template nginx-template \
         --target-pool nginx-pool
    ```
    Create a managed instance group using the instance template.
    This creates 2 virtual machine instances with names that are prefixed with `nginx-`. This may take a couple of minutes.
1. `gcloud compute instances list`
    List the compute engine instances.
1. `gcloud compute firewall-rules create www-firewall --allow tcp:80`
    Configure a firewall to connect to the machines on port 80 via EXTERNAL_IP address.
    Connect the instances via external IP 'http://EXTERNAL_IP/'

## Create a Network Load Balancer
1. Balance the load of your systems based on incoming IP protocol data, such as address, port, and protocol type.
    You also get some options that are not available, with HTTP(S) load balancing.
    For example, you can load balance additional TCP/UDP-based protocols such as SMTP traffic.
    And if your application is interested in TCP-connection-related characteristics, network load balancing allows your app to inspect the packets, where HTTP(S) load balancing does not.
1. Network Load Balancing is a regional, non-proxied load balancer.
1. [Reference: Setting Up Network Load Balancing](https://cloud.google.com/compute/docs/load-balancing/network/)
1. ```shell
    gcloud compute forwarding-rules create nginx-lb \
         --region us-central1 \
         --ports=80 \
         --target-pool nginx-pool
    ```
    Create a L3 network load balancer targeting your instance group.
1. `gcloud compute forwarding-rules list`
    List all Google Compute Engine forwarding rules in project.
    Output:
    ```
    NAME     REGION       IP_ADDRESS     IP_PROTOCOL TARGET
    nginx-lb us-central1 X.X.X.X        TCP         us-central1/targetPools/nginx-pool
    ```
1. You can then visit the load balancer from the browser http://IP_ADDRESS/ where IP_ADDRESS is the address shown as the result of running the previous command.

## Create a Http(s) Load Balancer
1. Provides global load balancing for HTTP(S) requests destined for your instances.
    You can configure URL rules that route some URLs to one set of instances and route other URLs to other instances. Requests are always routed to the instance group that is closest to the user, provided that group has enough capacity and is appropriate for the request. If the closest group does not have enough capacity, the request is sent to the closest group that does have capacity.
1. [Reference: HTTP(s) Load Balancer in the documentation](https://cloud.google.com/compute/docs/load-balancing/http/)
1. Overview:
    `LB Proxy` --(GlobalForwadingRules)--> `URL Maps` --(BackendService)--> `HealthCheck`;`GroupOfInstances(Port80)`
    | Element | Relationships |
    |----|----|
    | http-health-check | Port80 |
    | instance-groups | Port80 |
    | backend-services | http-health-check, instance-group |
    | url-maps | backend-service |
    | target-http-proxies | url-map |
    | forwarding-rules | target-http-proxy |
1. General steps:
    - Create an instance template
    - Create a target pool
    - Create a managed instance group
    - Create a firewall rule to allow traffic (80/tcp)
    - Create a health check
    - Create a backend service and attach the manged instance group
    - Create a URL map and target HTTP proxy to route requests to your URL map
    - Create a forwarding rule
1. `gcloud compute http-health-checks create http-basic-check`
    [Health check](https://cloud.google.com/compute/docs/load-balancing/health-checks) to verify the instance is responding to HTTP or HTTPS traffic.
1. ```shell
    gcloud compute instance-groups managed \
       set-named-ports nginx-group \
       --named-ports http:80
    ```
    Define an HTTP service and map a port name to the relevant port for the instance group. Now the load balancing service can forward traffic to the named port.
1. ```
    gcloud compute backend-services create nginx-backend \
    --protocol HTTP --http-health-checks http-basic-check --global
    ```
    Create a [backend service](https://cloud.google.com/compute/docs/reference/latest/backendServices).
1. ```
    gcloud compute backend-services add-backend nginx-backend \
    --instance-group nginx-group \
    --instance-group-zone us-central1-a \
    --global
    ```
    Add the instance group into the backend service.
1. ```
    gcloud compute url-maps create web-map \
    --default-service nginx-backend
    ```
    Create a default URL map that directs all incoming requests to all your instances.
    To direct traffic to different instances based on the URL being requested, see [content-based rounting](https://cloud.google.com/compute/docs/load-balancing/http/content-based-example).
1. ```
    gcloud compute target-http-proxies create http-lb-proxy \
    --url-map web-map
    ```
    Create a target HTTP proxy to route requests to your URL map
1. ```
    gcloud compute forwarding-rules create http-content-rule \
        --global \
        --target-http-proxy http-lb-proxy \
        --ports 80
    ```
    Create a global forwarding rule to handle and route incoming requests. A forwarding rule sends traffic to a specific target HTTP or HTTPS proxy depending on the IP address, IP protocol, and port specified. The global forwarding rule does not support multiple ports.
1. `gcloud compute forwarding-rules list`
